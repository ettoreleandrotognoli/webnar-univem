---
marp: true
theme: uncover
---

<style>
section {
    text-align: left;
    font-size: 28px;
}
ul {
    margin-left: 0px;
}
section.center p, section.center ul, section.center li,
section.center h1,  section.center h2, section.center h3,
section.center h4,  section.center h5, section.center h6  {
    text-align: center;
}
section h1 {
    font-size: 40px;
}
section::after {
  font-weight: bold;
}
section.cover header{
    text-align: right;
    opacity: 0.1;
}
</style>

<!-- _class: center invert cover -->
# Carreira Full Stack

<style scoped>
    section h1 {
        font-size: 55px;
    }
    footer {
        text-align: right;
    }
</style>

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->

Éttore Leandro Tognoli
ettore.leandro.tognoli@gmail.com

Bacharel em Ciência da Computação - UNIVEM
Mestre em Ciência da Computação - UFSCar
Engenheiro de Software - RedHat

<!-- _footer: Powered by [Marp](https://yhatt.github.io/marp/) & [PlantUML](https://github.com/plantuml) -->

---
<!-- _class: center invert -->

Não estou aqui representando a RedHat

---

<!-- paginate: true -->


# Agenda

Full Stack
Carreira
Mercado Internacional

---
<!-- _class: center -->
<!-- header: Full Stack -->


# Síndrome de Pato :duck:

O pato anda, nada e voa, mas não faz nada bem.

---
<!-- _class: center -->
<!-- header: Full Stack -->

Pelo meno o pato ainda existe

# :duck: > :dragon:

---

# Full Stack

Capaz de desenvolver toda a aplicação:


- Servidor / Backend
- Cliente / Frontend
- Banco de Dados
- Cloud

---


# Desenvolvimento WEB

- Frontend ( Executado no lado do Cliente )
    HTML, CSS, Bootstrap, W3.CSS, JavaScript, ES5, HTML DOM, JSON, XML, jQuery, Angular, React, Backbone.js, Express.js, Ember.js, Redux, Storybook, GraphQL, Meteor.js, Grunt, Gulp

- Backend ( Executado no lado do Servidor )
    PHP, ASP, C++, C#, Java, Python, Node.js, Ruby, REST, GO, SQL, MongoDB, Firebase, Sass, Less, Parse, PaaS

<!-- _footer: https://www.w3schools.com/whatis/whatis_fullstack.asp -->

---


# Desenvolvimento

- Web
- Mobile
  Android, NativeScript, Ionic
- Desktop
  Electron, Gtk, Qt, Unity  

---

# Qual é a stack?

Angular + Java Spring + MySQL
VueJS + PHP Laravel + MongoDB
React + Python Django + PostgreSQL

---

# Qual é a stack?

Angular + Vue + React + ...

Java Spring + PHP Laravel + Python Django + ...

Apache Kafka + Kubernetes + AWS / Google Cloud + ...

Cassandra + PostgreSQL + ...

---

![bg 90%](resources/languages.svg)

---

# Programação

VCS - Version Control System
## GIT

"Linus Torvalds transformed technology twice — first with the Linux kernel, which helps power the Internet, and again with Git, the source code management system used by developers worldwide."

<!-- _footer: https://www.youtube.com/watch?v=o8NPllzkFhE -->

---

# Programação

POO
TDD
SOLID
Design Patterns
Clean Code
Dependency Management

---

<!-- _header: Dependency Management -->

![bg 90%](resources/dependency-management.svg)

---

Clean Code

- Use as IDEs a seu favor
    - Auto completar
    - Refatoração

<!-- _footer: https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882 -->

---

# DevOps

Dev - Developer
Ops - Operations

Desenvolvimento ágil envolve entrega contínua, tornando inviável operações manuais, logo o processo de entrega precisa ser automatizado.

<!-- _footer: https://agilemanifesto.org/ -->

---

# Ferramentas de CI/CD

Continuos Delivery
Continuos Deployment
Continuos Integration

Infrastructure as Code ( IaC )

---
Scrum

Equipe Multidisciplinar

<!-- _footer: https://www.amazon.com.br/Scrum-Fazer-Dobro-Trabalho-Metade/dp/8544104517 -->

---

<!-- _class: center -->
<style scoped>
    section h1 {
        margin-top: -400px !important;
    }
</style>

# Especialista, Iniciante, Full Stack, Acomodado

![bg 70%](resources/knowledge.svg)

<!-- _footer: http://matt.might.net/articles/phd-school-in-pictures/ -->

---

# Carreira Full Stack

Não conheço a "receita de bolo"

Vou contar como foi a minha trajetória

---

<!-- header: Minha Carreira -->


# SENAI - Mecânica Automobilística

**PIC16 Assembly**, **C**, **Basic**

---

# UNIVEM - Bacharelado em Ciência da Computação

C/**C++**, **Java**, **PHP**, **VHDL**, **SQL**

Não somente com conteúdo das aulas, mas também com estágios, iniciação cientifica, trabalhando e fazendo TCC.

---

# Tray - Programador Jr.

**SVN**

**PHP**

**CakePHP**

---

# Guess - Informal

**Delphi**   :pensive:

SVN

---

# UFSCar - Mestrado em Ciência da Computação

**Verilog**, **C/C++**
**Git**

Oscilando entre CLT e PJ

---

# Telecontrol - Programador Jr.

Git, **Composer**

PHP, PostgreSQL

**Bootstrap**

**Docker**

---

# Autônomo, Passa-Tempos

Persys, Jacto, TecnoSuper, MyMedi
Git, **Virtualenv**
PHP, **Python**, SQLite
**Django**, **Android**, **Arduino**
**Jenkins**, **Heroku**, Docker

<!-- _footer: https://github.com/ettoreleandrotognoli/python-ami -->

---

# Frete Fácil ( ou algo do tipo )- Não Lembro

Fiquei dois dias e meio e não voltei mais 

---

# CIAg - Pesquisador Tecnológico

Java, **Kotlin**, Python, **TypeScript**, JavaScript, C/C++, MySQL, **MongoDB**

**VRaptor**, **Spring**, **Angular**, Django, Arduino, Android, **Ionic**, **NativeScript**

Git, **Pipenv**, **npm**, **sonarqube**

Jenkins, **GitLab Pipelines**, Docker, **OpenStack**, **Ansible**, **Terraform**, **Chef**, **AWS**

<!-- _footer: https://openciag.github.io -->

---

# RedHat - Engenheiro de Software

Java, Python, shell

Ansible, Terraform, Jenkins

**Git**, **Maven**


---

# O que eu acho que me ajudou

- Arriscar
    Não ter medo ou preconceito de novas tecnologias
    Não fique acomodado no seu trabalho

- Curiosidade
    Ir além do básico
    Não ficar satisfeito com meus conhecimentos

---

# 

Dicas do Roberto e do Daniel

Inglês, Linux e Indicação

---

<!-- header: Mercado Internacional -->

# Mercado Internacional

COVID-19 não está ajudando :mask:

[Vagas na RedHat](https://careers-redhat.icims.com/jobs/search)
[Vagas no Google](https://careers.google.com/jobs/)
[Vagas na Microsoft](https://careers.microsoft.com/us/en)

---

# Principais Diferenças

Funcionários Motivados

Gestores tem conhecimento técnico

Processos e Comunicação

---

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->
<!-- paginate: false -->
<!-- _class: center invert cover -->


# Obrigado!
